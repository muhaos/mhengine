/*
 * GOSound.cpp
 *
 *  Created on: 31.07.2010
 *      Author: �������������
 */

#include "GOSound.h"
#include "../SoundCache.h"


GOSound::GOSound() : GameObject() {
	init();
}


GOSound::GOSound(string id) : GameObject(id) {
	init();
	bindLua();
}


GOSound::GOSound(FILE* f) : GameObject(f) {

}


void GOSound::init() {
	canPlay = false;
	soundName = "";

    currentVolume = 1.0f;
    needFadein = false;
    needFadeout = false;

	alGenSources(1, &source);
    ALfloat sourceVel[] = { 0.0, 0.0, 0.0 };
    alSourcef(source, AL_PITCH,    1.0f     );
    alSourcef(source, AL_GAIN,     currentVolume);
    alSourcefv(source, AL_VELOCITY, sourceVel);
	alSourcei(source, AL_LOOPING,  false);
    
    ALenum error = alGetError();
	if (error != AL_NO_ERROR)
    	Log::warning("error create sound (%s)", alutGetErrorString(error));
    
    addToWorld();
}


void GOSound::bindLua() {

}


void GOSound::setSound(string name) {
	soundData = SoundCache::getInstance()->load(name);
	if (soundData) {
		soundName = name;
		alSourcei (source, AL_BUFFER, soundData->getBuffer());
		canPlay = true;
	} else {
		soundName = "";
		canPlay = false;
	}
}


void GOSound::setVolume(float new_volume) {
    currentVolume = new_volume;
    alSourcef(source, AL_GAIN,     currentVolume);
}


float GOSound::getVolume() {
    return currentVolume;
}


string GOSound::getSoundName() {
	return soundName;
}


void GOSound::setLoopCount(int count) { // -1 - one time play, 0 - infinity play, X = X play. Default -1
	if (count == -1)
		alSourcei (source, AL_LOOPING,  false);
    if (count == 0)
		alSourcei (source, AL_LOOPING,  true);
}


void GOSound::play(float fading_duration) {
	if (canPlay) {
        if (fading_duration != 0.0f) { // fading
            fadingDT = (1.0f / Game::instance->osAdapter->getFixedTimeStep()) / fading_duration;
            fadingDT = fadingDT * getVolume();
            tmpVolume = 0.0f;
            needFadein = true;
            alSourcef(source, AL_GAIN, tmpVolume);
        }
        alSourcePlay(source);
    }
}


void GOSound::stop(float fading_duration) {
	if (canPlay) {
        if (fading_duration != 0.0f) { // fading
            fadingDT = (1.0f / Game::instance->osAdapter->getFixedTimeStep()) / fading_duration;
            fadingDT = -fadingDT * getVolume();
            tmpVolume = getVolume();
            needFadeout = true;
        } else {
            alSourceStop(source);
        }
    }
}


void GOSound::pause() {
	if (canPlay)
		alSourcePause(source);
}


bool GOSound::isPlaying() {
	return true;
}


void GOSound::draw() {

}


void GOSound::update(float dt) {
	GameObject::update(dt);
//	ALfloat sourcePos[] = { getPosition().x, getPosition().y, getPosition().z };
//    alSourcefv(source, AL_POSITION, sourcePos);
    
    //fading in
    if (needFadein) {
        tmpVolume += fadingDT * dt;
        if (tmpVolume >= getVolume()) {
            needFadein = false;
            setVolume(getVolume());
        }
        alSourcef(source, AL_GAIN, tmpVolume);
    }
    
    //fading out
    if (needFadeout) {
        tmpVolume += fadingDT * dt;
        if (tmpVolume <= 0.0f) {
            needFadeout = false;
            alSourceStop(source);
            tmpVolume = getVolume();
        }
        alSourcef(source, AL_GAIN, tmpVolume);
    }
}


void GOSound::save(FILE* f) {

}


GOSound::~GOSound() {
	// TODO Auto-generated destructor stub
}
