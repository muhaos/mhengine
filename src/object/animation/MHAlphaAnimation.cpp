/*
 * MHAlphaAnimation.cpp
 *
 *  Created on: 25.02.2011
 *      Author: �������������
 */

#include "MHAlphaAnimation.h"

MHAlphaAnimation::MHAlphaAnimation(float newAlpha, float duration) {
	alpha = newAlpha;
	this->duration = duration;
    progress = 0;
}


AnimationType MHAlphaAnimation::getAnimationType() {
	return AlphaAT;
}


void MHAlphaAnimation::onStart() {
	MHAnimation::onStart();
	oldAlpha = owner->getAlpha();
	ddt = (1.0f / Game::instance->osAdapter->getFixedTimeStep()) / duration;

	if (alpha < oldAlpha) {
		// decreasing
		ddt = ddt * (oldAlpha - alpha);
	} else {
		// increasing
		ddt = ddt * (alpha - oldAlpha);
	}
}


void MHAlphaAnimation::update(float dt) {
	MHAnimation::update(dt);
	if (paused) return;

	if (alpha < oldAlpha) {
		// decreasing
		owner->setAlpha(owner->getAlpha() - ddt * dt );
		if (owner->getAlpha() < alpha) {
			owner->setAlpha(alpha);
			finish();
		}
        progress = (oldAlpha - owner->getAlpha()) / (oldAlpha - alpha);
	} else {
		// increasing
		owner->setAlpha(owner->getAlpha() + ddt * dt );
		if (owner->getAlpha() > alpha) {
			owner->setAlpha(alpha);
			finish();
		}
        progress = (owner->getAlpha() - oldAlpha) / (alpha - oldAlpha);
	}
}


float MHAlphaAnimation::getProgress() {
    return progress;
}


MHAlphaAnimation::~MHAlphaAnimation() {
}
