/*
 * GOSound.h
 *
 *  Created on: 31.07.2010
 *      Author: �������������
 */

#ifndef GOSOUND_H_
#define GOSOUND_H_

#include "GameObject.h"
#include "../platform/openal.h"
#include "../SoundData.h"

class GOSound: public GameObject {
private:
	ALuint source;
	SoundData* soundData;
	bool canPlay;
	string soundName;
    float currentVolume; // GAIN
    
    float tmpVolume;
    float fadingDT;
    bool needFadein;
    bool needFadeout;
    
	void init();
	void bindLua();
public:

	void setSound(string name);
	string getSoundName();
	void setLoopCount(int count); //0 - infinity play, X = X play. Default 1
	void setVolume(float new_volume); // from 0 to 1
    float getVolume();
    void play(float fading_duration = 0.0f);
	void stop(float fading_duration = 0.0f);
	void pause();
	bool isPlaying();

	void draw();
	void update(float dt);
	void save(FILE* f);

	GOSound();
	GOSound(string id);
	GOSound(FILE* f);
	virtual ~GOSound();
};

#endif /* GOSOUND_H_ */
